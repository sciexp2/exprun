Introduction
============

SciExp²-ExpRun (aka *Scientific Experiment Exploration - Experiment Running*) provides a framework for easing the workflow of executing experiments that require orchestrating multiple processes in local and/or remote machines.

The contents of the framework are organized into:

* An extension of `spur <https://pypi.org/project/spur>`_, to run processes on
  both local and remote machines, with improvements on error handling and
  cleanup.

* Various helper functions to manage package installation, file copying, process
  management (pinning, setting limits, etc.), or CPU frequency and interrupt
  configuration, among others.


Installing
==========

The simplest way to install SciExp²-ExpRun is using the official package [#pypi]_ and a virtual environment [#venv]_. This will ensure you have a controlled local installation of a specific version for each of your projects, making sure your scripts always use the same SciExp²-ExpRun version to avoid any possible future version incompatibilities.

1. Create a virtual environment::

    python3 -m venv ~/my-exprun

  You can create as many as you want; e.g., one for each different version of the SciExp²-ExpDef package that you want to test.

2. Install SciExp²-ExpRun::

     . ~/my-exprun/bin/activate
     pip install sciexp2-exprun

   Or to install a specific version (version ``0.0.0`` in the example)::

     . ~/my-exprun/bin/activate
     pip install "sciexp2-exprun==0.0.0"

.. [#pypi] https://pypi.org/project/sciexp2-exprun
.. [#venv] https://docs.python.org/tutorial/venv.html


Running
=======

SciExp²-ExpRun and Make
~~~~~~~~~~~~~~~~~~~~~~~

In you use :program:`make` to automate the execution of your scripts, here is a simple snippet you can add to your ``Makefile`` (the example uses version ``0.0.0`` of SciExp²-ExpRun)::

  all: deps/exprun
        # run our script using exprun
  	( . deps/exprun/bin/activate && /path/to/my/script.py )

  # the ".done" file ensures a partial installation will not count as a success
  deps/exprun: deps/exprun/.done
  	$(RM) -R $@
        mkdir -p $(dir $@)
  	python3 -m venv $@
  	( . $@/bin/activate && pip install "sciexp2-exprun==0.0.0" )
  	touch $<


Debugging aids
~~~~~~~~~~~~~~

You can start your scripts with ``ipython --pdb /path/to/my/script.py`` to get into a debugging shell whenever an error occurs [#pdb]_. Sometimes it is also useful to start an IPython shell [#embed]_ somewhere deep in your code to interactively evaluate its current state::

  from IPython import embed
  embed()

Exiting the debugging shell will returning to normal execution.

.. [#pdb] http://ipython.org/ipython-doc/stable/interactive/reference.html#post-mortem-debugging
.. [#embed] http://ipython.org/ipython-doc/stable/interactive/reference.html#embedding-ipython
