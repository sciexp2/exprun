# -*- coding: utf-8 -*-

import sys, os

sys.path.append(os.path.abspath('..'))

import sciexp2.exprun

extensions = ['sphinx.ext.autodoc', 'sphinx.ext.todo', 'sphinx.ext.jsmath',
              'sphinx.ext.intersphinx', 'sphinx.ext.autosummary',
              'sphinx.ext.extlinks',
              'numpydoc' ]
jsmath_path = "/usr/share"
intersphinx_mapping = {
    'python': ('http://docs.python.org', None),
    'numpy': ('http://docs.scipy.org/doc/numpy', None),
    }
todo_include_todos = False
autosummary_generate = True
autoclass_content = "both"
autodoc_member_order = "bysource"

extlinks = {
    'code': (
        'https://gitlab.com/sciexp2/exprun/tree/master/%s',
        ''),
    'issue': (
        'https://gitlab.com/sciexp2/exprun/issues/%s',
        '#'),
    }

numpydoc_show_class_members = False


# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

source_suffix = '.rst'
master_doc = 'index'
project = u'SciExp²-ExpRun'
copyright = u' 2019, Lluís Vilanova'
version = sciexp2.exprun.__version__
release = sciexp2.exprun.__version__
exclude_trees = ['_build']
default_role = 'autolink'
add_module_names = False
pygments_style = 'sphinx'
html_theme = 'default'
html_last_updated_fmt = '%b %d, %Y'
html_show_sourcelink = False
htmlhelp_basename = 'SciExpdoc'
