Reference Guide
===============

.. currentmodule:: sciexp2

.. note::

   Input files for the examples shown in the documentation are available on the root directory of the source distribution.

.. autosummary::
   :toctree: _reference

   exprun.spur
   exprun.cpu
   exprun.files
   exprun.kernel
   exprun.process
   exprun.wait
   exprun.util
