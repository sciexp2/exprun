SciExp²-ExpRun documentation contents
=====================================


:Release: |version|
:Date: |today|

Contents:

.. toctree::
   :numbered:
   :maxdepth: 2

   introduction.rst
   reference.rst

.. toctree::
   :hidden:

   news.rst


Indices and tables
==================

* :ref:`news`
* :ref:`genindex`
* :ref:`modindex`
