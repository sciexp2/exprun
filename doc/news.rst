.. _news:

Changes in SciExp²-ExpRun
=========================

Here's a brief description of changes introduced on each version.


0.3.3
-----

.. rubric:: External compatibility breaks

.. rubric:: New features

.. rubric:: Improvements

.. rubric:: Bug fixes

* Fix typo in child process cleanups on `sciexp2.exprun.spur`.

.. rubric:: Documentation

.. rubric:: Internals


0.3.2
-----

.. rubric:: External compatibility breaks

.. rubric:: New features

.. rubric:: Improvements

.. rubric:: Bug fixes

* Silence child process cleanup hiccups from watchdog threads in `sciexp2.exprun.spur`.

.. rubric:: Documentation

.. rubric:: Internals


0.3.1
-----

.. rubric:: External compatibility breaks

.. rubric:: New features

.. rubric:: Improvements

* Send SIGTERM during child process cleanup in `sciexp2.exprun.spur`.

.. rubric:: Bug fixes

* Capture SIGTERM in `sciexp2.exprun.spur` to properly detect child process termination.

.. rubric:: Documentation

.. rubric:: Internals


0.3.0
-----

.. rubric:: External compatibility breaks

.. rubric:: New features

* Add timeout argument to `sciexp2.exprun.wait.stringio`.

.. rubric:: Improvements

.. rubric:: Bug fixes

* Fix child process cleanup in `sciexp2.exprun.spur`.

.. rubric:: Documentation

.. rubric:: Internals


0.2.1
-----

.. rubric:: External compatibility breaks

.. rubric:: New features

.. rubric:: Improvements

.. rubric:: Bug fixes

* Fix child process cleanup.
* Fix thread ID collection.

.. rubric:: Documentation

.. rubric:: Internals


0.2.0
-----

.. rubric:: External compatibility breaks

.. rubric:: New features

* Add `sciexp2.exprun.spur.with_signum` to allow passing signal numbers to the ``kill`` argument.

.. rubric:: Improvements

* In `sciexp2.exprun.spur`, handle `send_signal` method properly on all shell types (``kill`` argument overrides default action).

.. rubric:: Bug fixes

* Fix `sciexp2.exprun.wait.print_stringio` when using an output ``file`` argument.
* Fix `sciexp2.exprun.cpu.get_cpus` when specifying a HW thread number.

.. rubric:: Documentation

.. rubric:: Internals


0.1.6
-----

.. rubric:: External compatibility breaks

.. rubric:: New features

.. rubric:: Improvements

.. rubric:: Bug fixes

* Fix construction from other ssh shells in `sciexp2.exprun.spur.get_shell`.

.. rubric:: Documentation

.. rubric:: Internals


0.1.5
-----

.. rubric:: External compatibility breaks

.. rubric:: New features

.. rubric:: Improvements

* Do not require ``intel_pstate=disable`` when setting CPU frequency.
* Ignore failed pids in `sciexp2.exprun.process.cgroup_move`.

.. rubric:: Bug fixes

* Fix process output parsing in `sciexp2.exprun.cpu.set_freq` and `sciexp2.exprun.process.cgroup_pids`.
* Fix pidfile reading in `sciexp2.exprun.process.cgroup_pids`.

.. rubric:: Documentation

.. rubric:: Internals

* Use f-strings for better command readability.


0.1.4
-----

.. rubric:: External compatibility breaks

.. rubric:: New features

.. rubric:: Improvements

.. rubric:: Bug fixes

.. rubric:: Documentation

.. rubric:: Internals

* Fix packaging.


0.1.3
-----

.. rubric:: External compatibility breaks

.. rubric:: New features

.. rubric:: Improvements

.. rubric:: Bug fixes

.. rubric:: Documentation

.. rubric:: Internals

* Fix packaging.


0.1.2
-----

.. rubric:: External compatibility breaks

.. rubric:: New features

.. rubric:: Improvements

.. rubric:: Bug fixes

* Fix top-level module exporting.
* Fix ssh shell creation.
* Fix child process monitoring.
* Fix `sciexp2.exprun.files.rsync`.
* Replace ``hwloc-ls`` with ``hwloc-calc`` in `sciexp2.exprun.cpu.get_cpus`.
* Ensure process output has appropriate type in `sciexp2.exprun.cpu.set_irqs`.

.. rubric:: Documentation

.. rubric:: Internals

* Add missing pylint config file.


0.1.1
-----

.. rubric:: External compatibility breaks

.. rubric:: New features

.. rubric:: Improvements

.. rubric:: Bug fixes

.. rubric:: Documentation

.. rubric:: Internals

* Minor release fix.


0.1.0
-----

.. rubric:: External compatibility breaks

.. rubric:: New features

* Initial version.

.. rubric:: Improvements

.. rubric:: Bug fixes

.. rubric:: Documentation

.. rubric:: Internals
