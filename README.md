SciExp²-ExpRun
==============

SciExp²-ExpRun (aka *Scientific Experiment Exploration - Experiment Running*)
provides a framework for easing the workflow of executing experiments that
require orchestrating multiple processes in local and/or remote machines.

You can find links to the documentation and all other relevant information in:

  https://sciexp2-exprun.readthedocs.io


Copyright
=========

Copyright 2019-2020 Lluís Vilanova <llvilanovag@gmail.com>

Sciexp²-ExpRun is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

Sciexp²-ExpRun is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.
